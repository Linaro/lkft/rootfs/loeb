FROM debian:buster
LABEL maintainer="Daniel Díaz <daniel.diaz@linaro.org>"

ARG DEBIAN_FRONTEND=noninteractive

RUN echo 'locales locales/locales_to_be_generated multiselect C.UTF-8 UTF-8, en_US.UTF-8 UTF-8 ' | debconf-set-selections \
 && echo 'locales locales/default_environment_locale select en_US.UTF-8' | debconf-set-selections \
 && echo 'dash dash/sh boolean false' | debconf-set-selections \
 && apt-get update && apt-get install -y --no-install-recommends \
       build-essential chrpath cpio curl diffstat file gawk libssl-dev python python3 python3-distutils texinfo \
       libelf-dev \
       libiberty-dev libmpc-dev libyaml-dev \
       lz4 \
       gettext-base \
       git \
       gnupg \
       jq \
       locales \
       nfs-common \
       openssh-client \
       rsync \
       shellcheck \
       sudo \
       unzip \
       wget ca-certificates \
       zstd \
 && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 \
 && groupadd -g 2000 lkftuser \
 && useradd -m -s /bin/bash -u 2000 -g 2000 lkftuser \
 && echo 'lkftuser ALL = NOPASSWD: ALL' > /etc/sudoers.d/lkft \
 && chmod 440 /etc/sudoers.d/lkft \
 && mkdir -p /poky \
 && chown -R 2000:2000 /poky \
 && wget -q -O /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo \
 && chmod +x /usr/local/bin/*

COPY loeb /usr/local/bin/loeb
COPY user-swap.sh /usr/local/sbin/user-swap.sh

WORKDIR /poky
USER lkftuser
ENV LANG=en_US.UTF-8

RUN repo init -u https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest -b sumo \
 && repo sync

USER root
ENTRYPOINT ["/usr/local/sbin/user-swap.sh"]
