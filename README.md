# LKFT Open Embedded Builder (LOEB)

## What is it
LOEB is a tool to simplify the building of OpenEmbedded root file systems for
LKFT. It has common defaults and helps with doing just the needful in order to
build a root file system or individual packages.

## How to use

Download the `loeb` binary and mark as executable (suggestion: add to local
`$PATH`.) Set the MACHINE to one of the supported ones, either via the
`MACHINE` environment variable or the `-m` argument, and pass the Bitbake
command to execute:


```
$ export MACHINE=juno
$ loeb bitbake rpb-console-image-lkft
```
or:
```
loeb -m juno bitbake rpb-console-image-lkft
```

LOEB will fetch the manifest and all layers therein in the current directory, and will then proceed to execute the Bitbake command.

## OE Dependencies

The following packages need to be installed on the (Debian Buster) host system
for OpenEmbedded to build properly:
* build-essential
* ca-certificates
* chrpath
* cpio
* curl
* diffstat
* file
* gawk
* git
* gnupg
* jq
* libssl-dev
* locales
* openssh-client
* python
* python3
* python3-distutils
* rsync
* shellcheck
* texinfo
* wget

Bitbake requires an UTF-8 locale and cannot run as the `root` user.

## Arguments

LOEB takes the following arguments:
* `-m`: OpenEmbedded target machine to build. No default value.
* `-u`: URL for the `repo` OE manifest. Default:
  `https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest`
* `-b`: Branch for the `repo` OE manifest. Default: `sumo`
* `-c`: Location of both OE caches (downloads and sstate-cache). No default
  value.
* `-s`: Do a repo sync in the process. Defaults to no.
* `-d`: OE distro to use. Default: `lkft`.
* `-i`: Run an interactive LOEB session by entering the OE environment so that
  the Bitbake commands can be entered manually (and successively).
* `-q`: Reduce verbosity. Default is no.
