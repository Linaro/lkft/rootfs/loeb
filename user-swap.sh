#!/bin/bash

if [[ -v USERSWAP_DEBUG ]]; then
  set -x
fi

# $1: username
print_user_uid_gid() {
  id_a="$(id -a "$1")"
  this_user_uid="$(echo "${id_a}" | grep -Eo 'uid=[0-9]*' | cut -d= -f2)"
  this_user_gid="$(echo "${id_a}" | grep -Eo 'gid=[0-9]*' | cut -d= -f2)"
  echo "${this_user_uid}/${this_user_gid}"
}

# $1: dir
# $2: "uid" or "gid"
print_dir_id() {
  case "$2" in
    uid)
      stat_c="%u"
      ls_pos="\$3"
      ;;
    gid)
      stat_c="%g"
      ls_pos="\$4"
      ;;
    *)
      stat_c="%u/%g"
      ls_pos="\$3/\$4"
      ;;
  esac

  if [ -d "$1" ]; then
    if command -v stat > /dev/null; then
      stat -c "${stat_c}" "$1"
    else
      # shellcheck disable=SC2012
      ls -lnd "$1" | awk "{print ${ls_pos}}"
    fi
  fi
}

echo "== Current user UID/GID for LKFT User:"
IFS=/ read -r lkftuser_uid lkftuser_gid < <(print_user_uid_gid lkftuser)
echo "${lkftuser_uid}/${lkftuser_gid}"

user_uid=""
user_gid=""

# See which uid/gid was used for /poky/build
if [ -d /poky/build ]; then
  if [ -z "${user_uid}" ] || [ "${user_uid}" -eq "0" ]; then
    user_uid="$(print_dir_id /poky/build uid)"
  fi
  if [ -z "${user_gid}" ] || [ "${user_gid}" -eq "0" ]; then
    user_gid="$(print_dir_id /poky/build gid)"
  fi

  echo "== Host user UID/GID in /poky/build:"
  echo "${user_uid}/${user_gid}"
fi

# If /poky/build was created by Docker (root), retry with parent dir
if [ -d /poky ]; then
  if [ -z "${user_uid}" ] || [ "${user_uid}" -eq "0" ]; then
    user_uid="$(print_dir_id /poky uid)"
  fi
  if [ -z "${user_gid}" ] || [ "${user_gid}" -eq "0" ]; then
    user_gid="$(print_dir_id /poky gid)"
  fi

  echo "== Host user UID/GID in /poky:"
  echo "${user_uid}/${user_gid}"
fi

if [ -z "${user_uid}" ]; then user_uid="${lkftuser_uid}"; fi
if [ -z "${user_gid}" ]; then user_gid="${lkftuser_gid}"; fi

if [ ! "${user_gid}" = "0" ] && [ ! "${user_gid}" = "${lkftuser_gid}" ]; then
  echo "-> Changing gid to ${user_gid}"
  sudo sed -i -e "s#^lkftuser:x:[0-9]*:#lkftuser:x:${user_gid}:#" /etc/group
  sudo usermod -g "${user_gid}" lkftuser
fi

if [ ! "${user_uid}" = "0" ] && [ ! "${user_uid}" = "${lkftuser_uid}" ]; then
  echo "-> Changing uid to ${user_uid}"
  sudo usermod -u "${user_uid}" lkftuser
fi

echo

if [ "${user_gid}" != "${lkftuser_gid}" ] || [ "${user_uid}" != "${lkftuser_uid}" ]; then
  echo "== New info about LKFT User:"
  print_user_uid_gid lkftuser
  echo "Chowning..."
  sudo chown -R lkftuser:lkftuser /poky
else
  echo "== LKFT User remains unchanged"
fi

#su - lkftuser -c /bin/bash -- "$@"
#exec /bin/bash "$@"
#sudo -u lkftuser /bin/bash "$@"

echo "== Parameters: [$*]"
if [ $# -eq 0 ] || [ "${CI}" = "true" ]; then
  start-stop-daemon --pidfile /dev/null --start --chuid "$(id -u lkftuser)" --chdir /poky --startas /bin/bash
else
  # Special case: shells
  if [ "$1" == "sh" ] || [ "$1" == "bash" ]; then
    args=(-c "$@")
  else
    args=("$@")
  fi
  start-stop-daemon --pidfile /dev/null --start --chuid "$(id -u lkftuser)" --chdir /poky --startas /bin/bash -- "${args[@]}"
fi
